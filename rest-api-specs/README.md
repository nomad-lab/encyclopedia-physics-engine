REST API specifications for Encyclopedia Engine
===

The specifications are done using API Blueprint. API Blueprint - https://apiblueprint.org - is an open sourced documentation-oriented web API description language. The API Blueprint is essentially a set of semantic assumptions laid on top of the Markdown syntax used to describe a web API.

In addition to the regular Markdown syntax, API Blueprint conforms to the GitHub Flavored Markdown syntax.

## Generating the documentation

aglio - https://github.com/danielgtaylor/aglio - is used for generating the static html file. Aglio is an API Blueprint renderer with theme support that outputs static HTML. The generated HTML page is self contained and can be shared with different people if needed.

Install aglio via NPM. Node.js has to be installed and may need to use sudo to install it globally:
~~~
npm install -g aglio
~~~

Run it:
~~~
aglio -i rest-api-specs.apib -o rest-api-specs.html
~~~

To apply a new color schema (to match Encyclopedia colors), create a new .less file where the variables can be defined, and use it when the final html is generated:
~~~
aglio --theme-variables nomad-colors-custom.less -i rest-api-specs.apib -o rest-api-specs.html
~~~

Open the generated HTML file **rest-api-specs.html** in your web browser.
