# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from elasticsearch import Elasticsearch
from flask import Flask
from flask_httpauth import HTTPBasicAuth

from app.dmss import DMSS
from app.config import dmss_config, es_config

es = Elasticsearch([es_config['host']], port=es_config['port'])
dmss = DMSS()

http_auth = HTTPBasicAuth()


def create_app():
    logging.basicConfig(level=logging.INFO)

    """Create an application instance."""
    app = Flask(__name__)

    # apply configuration
    app.config.from_object('app.config')

    # initialize extensions
    app.logger.info("Used DMSS config: %s " % str(dmss_config))
    app.logger.info("Used ES config: %s" % str(es_config))

    # register blueprints
    from .api_v1_0 import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/v1.0')

    return app


app = create_app()
