# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
import functools
import urllib.parse as urlparse
from flask import request
from urllib.parse import urlencode


def paginate_queryparams(func):
    def analyze_queryparams():
        if request.args.get('pagination') == 'off':
            return False, None, None
        else:
            return True, request.args.get('page'), request.args.get('per_page')

    return paginate(analyze_queryparams)(func)


def paginate(analyze_request):
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            paginate, page, per_page = analyze_request()
            if page is not None:
                page = int(page)
            if per_page is not None:
                per_page = int(per_page)

            additional_keys = {}
            if paginate:
                if page is None or page < 1:
                    page = 1
                if per_page is None or per_page < 1:
                    per_page = 25

                kwargs["page"] = page
                kwargs["per_page"] = per_page

                func_results = func(*args, **kwargs)

                assert len(func_results) == 2 or len(func_results) == 3
                results, total_results = [], 0
                if len(func_results) == 2:
                    results, total_results = func_results
                elif len(func_results) == 3:
                    results, total_results, additional_keys = func_results

                total_pages = math.ceil(total_results / per_page)
                pages = {
                    "page": page,
                    "per_page": per_page,
                    "pages": total_pages,
                    "total": total_results
                }

                def url(page):
                    url = request.url

                    url_parts = list(urlparse.urlparse(url))
                    query = dict(urlparse.parse_qsl(url_parts[4]))
                    query["page"] = page

                    url_parts[4] = urlencode(query)

                    return urlparse.urlunparse(url_parts)

                if request.method == 'GET':
                    if page > 1:
                        pages['prev_url'] = url(page - 1)
                    if page < total_pages:
                        pages['next_url'] = url(page + 1)
                    pages['first_url'] = url(1)
                    pages['last_url'] = url(total_pages)

                result_dict = dict(results=results, pages=pages, total_results=total_results)
            else:
                kwargs["page"] = 1
                kwargs["per_page"] = 10000

                results, total_results = func(*args, **kwargs)
                result_dict = dict(results=results, total_results=total_results)

            for key, value in additional_keys.items():
                result_dict[key] = value

            return result_dict

        return wrapped
    return decorator
