# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired

"""List of atomic symbols in order."""
_symbols = [
    None, 'H',  'He', 'Li', 'Be',
    'B',  'C',  'N',  'O',  'F',
    'Ne', 'Na', 'Mg', 'Al', 'Si',
    'P',  'S',  'Cl', 'Ar', 'K',
    'Ca', 'Sc', 'Ti', 'V',  'Cr',
    'Mn', 'Fe', 'Co', 'Ni', 'Cu',
    'Zn', 'Ga', 'Ge', 'As', 'Se',
    'Br', 'Kr', 'Rb', 'Sr', 'Y',
    'Zr', 'Nb', 'Mo', 'Tc', 'Ru',
    'Rh', 'Pd', 'Ag', 'Cd', 'In',
    'Sn', 'Sb', 'Te', 'I',  'Xe',
    'Cs', 'Ba', 'La', 'Ce', 'Pr',
    'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
    'Tb', 'Dy', 'Ho', 'Er', 'Tm',
    'Yb', 'Lu', 'Hf', 'Ta', 'W',
    'Re', 'Os', 'Ir', 'Pt', 'Au',
    'Hg', 'Tl', 'Pb', 'Bi', 'Po',
    'At', 'Rn', 'Fr', 'Ra', 'Ac',
    'Th', 'Pa', 'U',  'Np', 'Pu',
    'Am', 'Cm', 'Bk', 'Cf', 'Es',
    'Fm', 'Md', 'No', 'Lr'
]

"""Dict of atomic symbols with their atomic number"""
_numbers = {
    'H': 1,  'He': 2, 'Li': 3, 'Be': 4,
    'B': 5,  'C': 6,  'N': 7,  'O': 8,  'F': 9,
    'Ne': 10, 'Na': 11, 'Mg': 12, 'Al': 13, 'Si': 14,
    'P': 15,  'S': 16,  'Cl': 17, 'Ar': 18, 'K': 19,
    'Ca': 20, 'Sc': 21, 'Ti': 22, 'V': 23,  'Cr': 24,
    'Mn': 25, 'Fe': 26, 'Co': 27, 'Ni': 28, 'Cu': 29,
    'Zn': 30, 'Ga': 31, 'Ge': 32, 'As': 33, 'Se': 34,
    'Br': 35, 'Kr': 36, 'Rb': 37, 'Sr': 38, 'Y': 39,
    'Zr': 40, 'Nb': 41, 'Mo': 42, 'Tc': 43, 'Ru': 44,
    'Rh': 45, 'Pd': 46, 'Ag': 47, 'Cd': 48, 'In': 49,
    'Sn': 50, 'Sb': 51, 'Te': 52, 'I': 53,  'Xe': 54,
    'Cs': 55, 'Ba': 56, 'La': 57, 'Ce': 58, 'Pr': 59,
    'Nd': 60, 'Pm': 61, 'Sm': 62, 'Eu': 63, 'Gd': 64,
    'Tb': 65, 'Dy': 66, 'Ho': 67, 'Er': 68, 'Tm': 69,
    'Yb': 70, 'Lu': 71, 'Hf': 72, 'Ta': 73, 'W': 74,
    'Re': 75, 'Os': 76, 'Ir': 77, 'Pt': 78, 'Au': 79,
    'Hg': 80, 'Tl': 81, 'Pb': 82, 'Bi': 83, 'Po': 84,
    'At': 85, 'Rn': 86, 'Fr': 87, 'Ra': 88, 'Ac': 89,
    'Th': 90, 'Pa': 91, 'U': 92,  'Np': 93, 'Pu': 94,
    'Am': 95, 'Cm': 96, 'Bk': 97, 'Cf': 98, 'Es': 99,
    'Fm': 100, 'Md': 101, 'No': 102, 'Lr': 103,
    'Rf': 104, 'Db': 105, 'Sg': 106, 'Bh': 107, 'Hs': 108,
    'Mt': 109, 'Ds': 110, 'Rg': 111, 'Cn': 112, 'Nh': 113
}


def sort_elements(elements):
    return sorted(elements, key=lambda element: _numbers.get(element.rstrip('0123456789'), 200))


def get_atom_number(symbol):
    """Given an element symbol, return the atomic number (number of protons).

    :param symbol: The atomic symbol for the given element.
    :type symbol: str

    :return: The atomic number (number of protons) of the element. If no
        species could be found, returns None.
    :rtype: int
    """
    number = _numbers.get(symbol)
    return number


def get_atom_symbol(number):
    """Given an atomic number, returns the symbol for that element.

    :param number: The atomic number (number of protons) of the element.
    :type number: int

    :return: The atomic symbol for the given element. If no species could be
        found, returns None.
    :rtype: str
    """
    try:
        symbol = _symbols[number]
    except IndexError:
        symbol = None

    return symbol

########################################################################
# Calculate the formula using the same algorithm as in enc-preprocessing
########################################################################


def format_el_count(element, element_count):
    """Given an element and the number of these elements in the system, returns
    a string with the element symbol and number concatenated for use in the Hill
    system notation.

    :param element: Element symbol.
    :param element_count: Number of elements.
    :type element: string
    :type element_count: int

    :return: The element and number combined.
    :rtype: string
    """
    if element_count > 1:
        return "%s%d" % (element, element_count)
    else:
        return element


def sum_form(atom_labels):
    """Given a list of atomic labels, returns the chemical formula using the
    Hill system (https://en.wikipedia.org/wiki/Hill_system).

    :param atom_labels: Atom labels.
    :type original_cell: array of strings

    :return: The chemical formula using Hill system.
    :rtype: list
    """

    # Examples:
    # >>> sum_form(["Rh", "Si", "Te"])
    # 'RhSiTe'
    # >>> sum_form(["Mn", "Mn", "H", "He"])
    # 'HHeMn2'

    if atom_labels is None:
        return None

    # Count occurancy of elements
    re_element = re.compile(r"^([A-Za-z]+)")
    element_count = {}
    for label in atom_labels:
        element = re_element.match(label).group(1)
        if len(element) < 1:
            print("Invalid atom label '{}' when creating chemical formula.".format(label))
        if element in element_count:
            element_count[element] += 1
        else:
            element_count[element] = 1

    # Apply basic Hill system:
    # 1. Is Carbon part of the system?
    sum_form_start = []
    sum_form_end = []
    has_carbon = 1
    try:
        sum_form_start.append(format_el_count('C', element_count['C']))
        del element_count['C']
    except KeyError:
        has_carbon = 0
    if has_carbon:
        # 1a. add hydrogen
        try:
            sum_form_start.append(format_el_count('H', element_count['H']))
            del element_count['H']
        except KeyError:
            pass
    # 2. all remaining elements in alphabetic order
    for element in sorted(element_count):
        sum_form_start.append(format_el_count(element, element_count[element]))
    # TODO: implement all the exceptions regarding ordering
    #       in chemical formulas:
    #         - ionic compounds (ordering wrt to ionization)
    #         - oxides, acids, hydroxides...
    return sum_form_start + sum_form_end


def split_and_order(search_formula):
    """Given a string as a formula returns a list of elements sorted based on
    the Hill system notation (https://en.wikipedia.org/wiki/Hill_system).

    :param formula: Atom labels.
    :type list: array of strings

    :return: A list with sorted elements.
    :rtype: list
    """

    # split the formula in individual elements
    element_list = re.findall("[A-Z][a-z]?\d*|\(.*?\)\d+", search_formula)

    carbons = []
    hydrogens = []
    final_list = []

    for element in element_list:
        if re.match(r'(C\d*$)', element):
            carbons.append(element)
        elif re.match(r'(H\d*$)', element):
            hydrogens.append(element)
        else:
            final_list.append(element)

    if len(carbons) == 0:
        final_list = sorted(final_list + hydrogens)
    elif len(hydrogens) == 0:
        final_list = carbons + sorted(final_list)
    else:
        final_list = carbons + hydrogens + sorted(final_list)

    return final_list


########################################################################
# Calculate the formula using the same algorithm as in Preprocessing
########################################################################

def form_list(names, counts):
    """Taking the output from hill_decomposition function, will return the
    formula.
    Example:
        (['Si', 'Te'], [1, 2]) -> ['Si', 'Te2']
        (['Ti', 'O'], [1, 1])  -> ['Ti', 'O']
        (['H', 'He', 'Mn'], [1, 1, 2]) -> ['H', 'He', 'Mn2']
        (['B', 'C'], [1, 1]) -> ['B', 'C']
    """

    formula = []
    for name, count in zip(names, counts):
        if count > 1:
            formula.append("%s%d" % (name, count))
        else:
            formula.append(name)
    return formula


def form_string(names, counts):
    """Taking the output from hill_decomposition function, will return the
    formula.

    Example:
    (['Si', 'Te'], [1, 2]) -> SiTe2
    (['Ti', 'O'], [1, 1])  -> TiO
    (['H', 'He', 'Mn'], [1, 1, 2]) -> HHeMn2
    (['B', 'C'], [1, 1]) -> BC
    """

    formula = ""
    for name, count in zip(names, counts):
        if count > 1:
            formula += "%s%d" % (name, count)
        else:
            formula += name
    return formula


def hill_decomposition(atom_labels):
    """Given a list of atomic labels, returns the chemical formula using the
    Hill system (https://en.wikipedia.org/wiki/Hill_system).

    :param atom_labels: Atom labels.
    :type original_cell: list of strings

    :return: The chemical formula using Hill system.
    :rtype: see the example below :)

    Example:
    input:
        ["Te", "Si", "Te"])
        ["Ti", "O"])
        ["Mn", "Mn", "H", "He"])
        ["B", "C"])
    output:
        (['Si', 'Te'], [1, 2])
        (['Ti', 'O'], [1, 1])
        (['H', 'He', 'Mn'], [1, 1, 2])
        (['B', 'C'], [1, 1])
    """

    if atom_labels is None:
        return None

    names = []
    counts = []

    # Count occurancy of elements
    re_element = re.compile(r"^([A-Za-z]+)")
    element_count = {}
    for label in atom_labels:
        element = re_element.match(label).group(1)
        if len(element) < 1:
            print("Invalid atom label '{}' when creating chemical formula.".format(label))
        if element in element_count:
            element_count[element] += 1
        else:
            element_count[element] = 1

    # Apply basic Hill system:
    # 1. Is Carbon part of the system?
    has_carbon = 1
    try:
        counts.append(element_count["C"])
        names.append("C")
        del element_count['C']
    except KeyError:
        has_carbon = 0
    if has_carbon:
        # 1a. add hydrogren
        try:
            n_hydrogen = element_count["H"]
            names.append("H")
            counts.append(n_hydrogen)
            del element_count['H']
        except KeyError:
            pass
    # 2. all remaining elements in alphabetic order
    for element in sorted(element_count):
        names.append(element)
        counts.append(element_count[element])

    # 3. Binary ionic compounds: cation first, anion second
    # If any of the most electronegative elements is first
    # by alphabetic order, we move it to second

    # order non-metallic elements by electronegativity
    order = {}

    order["F"] = 1
    order["O"] = 2
    order["N"] = 3
    order["Cl"] = 4
    order["Br"] = 5
    order["C"] = 6
    order["Se"] = 7
    order["S"] = 8
    order["I"] = 9
    order["As"] = 10
    order["H"] = 11
    order["P"] = 12
    order["Ge"] = 13
    order["Te"] = 14
    order["B"] = 15
    order["Sb"] = 16
    order["Po"] = 17
    order["Si"] = 18
    order["Bi"] = 19

    if len(counts) == 2:
        if (names[0] in order):
            if (names[1] in order):
                if (order[names[0]] < order[names[1]]):
                    # For non-metals:
                    # Swap symbols and counts if first element
                    # is more electronegative than the second one,
                    # because the more electronegative element is the anion
                    names[0], names[1] = names[1], names[0]
                    counts[0], counts[1] = counts[1], counts[0]
            else:
                # Swap symbols and counts always if second element
                # is any other element,i.e.,
                # put non-metal last because it is the anion
                names[0], names[1] = names[1], names[0]
                counts[0], counts[1] = counts[1], counts[0]

    # TODO: implement all further exceptions regarding ordering
    #       in chemical formulas:
    #         - ionic compounds (ordering wrt to ionization)
    #         - oxides, acids, hydroxides...

    return (names, counts)


def formula_hill_as_string(atom_labels):
    """Given a list of atomic labels, returns the chemical formula using the
    Hill system (https://en.wikipedia.org/wiki/Hill_system).

    :param atom_labels: Atom labels.
    :type: list of strings

    :return: The chemical formula using Hill system.
    :rtype: string
    """

    names, counts = hill_decomposition(atom_labels)
    return form_string(names, counts)


def formula_hill_as_list(atom_labels):
    """Given a list of atomic labels, returns the chemical formula using the
    Hill system (https://en.wikipedia.org/wiki/Hill_system).

    :param atom_labels: Atom labels.
    :type: list of strings

    :return: The chemical formula using Hill system.
    :rtype: list
    """

    names, counts = hill_decomposition(atom_labels)
    return form_list(names, counts)


def reformat_user_formula(search_formula):
    """Takes a user's formula and reformat it using Hill system
    :param search_formula:
    :return: list
    """

    element_list = re.findall("[A-Z][a-z]?\d*|\(.*?\)\d+", search_formula)
    r = re.compile("([a-zA-Z]+)([0-9]+)")
    final_list = []
    for element in element_list:
        m = r.match(element)
        if m:
            # first group contains the element and second group the counting
            final_list += [m.group(1)]*int(m.group(2))
        else:
            final_list.append(element)

    return formula_hill_as_list(final_list)


def flag_to_gitlab(title, description, label):
    """ Create a new issue to our GitLab issue tracker for review
    """

    import requests

    from app import create_app
    app = create_app()
    with app.app_context():
        # the issue will be opened in nomad-lab/encyclopedia-general project
        # right now a temporary TOKEN is used, valid for only a few days
        issue = requests.post("https://gitlab.mpcdf.mpg.de/api/v4/projects/48/issues",
                          data={
                              "title": title,
                              "description": description,
                              "labels": label
                          },
                          headers={"PRIVATE-TOKEN": "4Ab-9y3_QJTRFcM89Bn5"})
        # print(issue.status_code, issue.reason)


# Two methods for dealing with token generation and verification
def generate_auth_token(user, expiration=3600):
    # default token expiration time set to 10 minutes
    # useful values: 86400 = 24h, 43200 = 12h, 3600 = 1h
    # 1 year = 365.25 days = (365.25 days) (24 hours/day) (3600 seconds/hour) = 31557600 seconds
    s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps({'id': user})


def verify_auth_token(token):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        # the token is valid, but expired
        return None
    except BadSignature:
        # the token is invalid
        return None
    user = data['id']
    return user
