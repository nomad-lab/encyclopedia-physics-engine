# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class InvalidEncRequest(Exception):

    def __init__(self, message, status_code, more=None):
        super().__init__(message)
        self._message = message
        self._more = more
        self.status_code = status_code

    def to_dict(self):
        the_dict = dict(status=self.status_code, error=self._message)
        if self._more is not None:
            for key, value in self._more.items():
                the_dict[key] = value
        return the_dict

