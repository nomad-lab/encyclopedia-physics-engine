# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import jsonify
from . import api


# this has to be an app-wide handler
@api.app_errorhandler(404)
def not_found(e):
    response = jsonify({'status': 404, 'error': 'not found',
                        'message': 'invalid resource URI'})
    response.status_code = 404
    return response


# this has to be an app-wide handler
@api.app_errorhandler(405)
def method_not_supported(e):
    response = jsonify({'status': 405, 'error': 'method not supported',
                        'message': 'the method is not supported'})
    response.status_code = 405
    return response


# this has to be an app-wide handler
@api.app_errorhandler(500)
def internal_server_error(e):
    response = jsonify({'status': 500, 'error': 'internal server error',
                        'message': e.args[0]})
    response.status_code = 500
    return response


# needed for example when searching by elements/formula
# but not using exclusive parameter
@api.app_errorhandler(400)
def bad_request(e):
    response = jsonify({'status': 400, 'error': 'bad request',
                        'message': 'Bad request'})
    response.status_code = 400
    return response
