# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# needed for SAML authentication
# BEGIN here
import os
import socket
from flask import redirect, session, url_for, logging
from flask import request, jsonify, make_response
from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.utils import OneLogin_Saml2_Utils
# needed for RQ and RQScheduler
from redis import Redis
from urllib.parse import urlparse
import re
from elasticsearch import TransportError


from app import es_config
from . import api
from .. import http_auth, es, dmss
from ..decorators import json
from ..decorators.paginate import paginate_queryparams, paginate
from ..dmss import diagram_data_types
from ..errors import InvalidEncRequest
from ..utils import generate_auth_token, verify_auth_token, flag_to_gitlab, reformat_user_formula, hill_decomposition, sort_elements

LOGGER = logging.getLogger(__name__)

# from rq import Queue
# # for online server I need AUTH for redis
# # if 'enc-staging-nomad' in os.uname().nodename:
# #     q = Queue(connection=Redis(password='My_Pass'))
# # else:
# q = Queue(connection=Redis())

# based on machine's hostname use different config files for SAML
# TODO switch to use environment variables: PRODUCTION, STAGING, DEVELOPMENT
hostname = socket.gethostname()
if hostname == 'enc-production-nomad':
    saml_config_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'saml-production')
elif hostname == 'enc-production-hv-nomad':
    saml_config_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'saml-production-hv')
elif hostname == 'enc-staging-nomad':
    saml_config_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'saml-staging')
elif 'production' in hostname:
    saml_config_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'saml-production')
else:
    saml_config_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'saml-testing')



def init_saml_auth(req):
    auth = OneLogin_Saml2_Auth(req, custom_base_path=saml_config_dir)
    return auth


def prepare_flask_request(request):
    # If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
    url_data = urlparse(request.url)
    # On staging machine we still have HTTP by default so, I want the API app to
    # work when using SAML authentication with the IdP provider. That's why I need
    # different settings for this function
    if 'production' in hostname:
        return {
            'https': 'on' if request.scheme == 'https' else 'off',
            'http_host': request.host + '/api',
            'server_port': 443,
            'script_name': request.path,
            'get_data': request.args.copy(),
            # Uncomment if using ADFS as IdP, https://github.com/onelogin/python-saml/pull/144
            # 'lowercase_urlencoding': True,
            'post_data': request.form.copy()
        }
    else:
        return {
            'https': 'on' if request.scheme == 'https' else 'off',
            'http_host': request.host,
            'server_port': url_data.port,
            'script_name': request.path,
            'get_data': request.args.copy(),
            # Uncomment if using ADFS as IdP, https://github.com/onelogin/python-saml/pull/144
            # 'lowercase_urlencoding': True,
            'post_data': request.form.copy()
        }
# END here


# needed for HTTPAuth
# BEGIN here
@http_auth.verify_password
def verify_password(token, unused):
    from .. import app
    if app.testing:
        return True

    # if token:
    user = verify_auth_token(token)
    if user:
        return True
    return False


@http_auth.error_handler
def unauthorized_token():
    response = jsonify({'status': 401, 'error': 'unauthorized',
                        'message': 'please send your authentication token',
                        'login_url': url_for('api.index', _external=True)})
    response.status_code = 401
    return response
# END here


############################
# Routes - URI
############################


@api.route('/')
@http_auth.login_required
def show_all():
    return make_response(jsonify({'error': 'Resource not found'}), 404)


def analyze_searchdata():
    json_data = request.get_json()
    if not json_data:
        raise InvalidEncRequest("No input data provided", 400)
    search_by = json_data.get('search_by')
    if search_by is None:
        raise InvalidEncRequest("Key search_by is mandatory", 400)
    return True, search_by.get('page', 1), search_by.get('per_page', 25)


def render_es_result(result):
    fields = [
        'formula_cell',
        'formula_reduced',
        'atom_labels_keyword',
        'material_id',
        'material_name',
        'calculations',
        'space_group_number',
        'space_group_international_short_symbol',
        'strukturbericht_designation',
        'structure_type',
        'structure_prototype',
        'system_type',
        'crystal_system',
        'basis_set_type',
        'functional_type'

    ]
    renames = {
        'material_id': 'id',
        'atom_labels_keyword': 'atom_labels',
        'calculations': 'nr_of_calculations'
    }
    legacy_values = {
        'formula': result.get('formula_cell')
    }

    es_result = {renames.get(field, field): result.get(field) for field in fields}
    es_result.update(**legacy_values)

    return es_result

_atom_label_with_number = re.compile("[A-Z][a-z]*[0-9]+")

@api.route('/materials', methods=['POST'])
@http_auth.login_required
@json
@paginate(analyze_searchdata)
def get_materials(page, per_page):
    json_data = request.get_json()
    if not json_data:
        raise InvalidEncRequest("No input data provided", 400)
    
    # check for ecxeeded elasticsearch pagination limit
    scroll = json_data.get('search_by').get('pagination') == 'scroll'
    if page*per_page >= 1e4 and not scroll:
        raise InvalidEncRequest('Too many results requested. Limit is 10000, or use "pagination":"scoll" and subsequently scroll_id.', 510)

    # create the match part
    match_query = {}
    search_by = json_data.get('search_by')
    exclusive = True if search_by.get('exclusive', '1') == '1' else False
    user_formula = search_by.get('formula')
    user_elements = search_by.get('element')
    elements = None
    if user_formula is not None:
        parts = reformat_user_formula(user_formula)
        parts = [part if _atom_label_with_number.match(part) else '%s1' % part for part in parts]
        if exclusive:
            match_query['formula_reduced_keyword'] = dict(query="".join(sort_elements(parts)))
        else:
            match_query['formula_reduced_terms'] = dict(query=" ".join(parts), operator='and')

    elif user_elements is not None:
        try:
            elements, _ = hill_decomposition(user_elements.split(","))
        except Exception:
            raise InvalidEncRequest("Could not parse given elements %s." % user_elements, 400)

        if elements is not None:
            if exclusive:
                match_query['atom_labels_keyword'] = dict(query="".join(sort_elements(elements)))
            else:
                match_query['atom_labels_terms'] = dict(query=" ".join(elements), operator='and')

    # create filters
    filter_queries = []

    def create_range_query(field, value):
        assert isinstance(value, dict)
        max = value.get('max')
        min = value.get('min')
        assert max is not None and min is not None
        return {
            "script": {
                "script": {
                    "source": "!(doc['%s'][0] > %f || doc['%s'][1] < %f)" % (field, max, field, min),
                    "lang": "painless"
                }
            }
        }

    def create_terms_query(field, values):
        assert isinstance(values, list)
        return {
            "terms": {
                field: values
            }
        }

    def create_bool_query(field, value):
        value = True if value == "Yes" else False if value == "No" else value
        assert isinstance(value, bool)
        if value:
            return {
                "exists": {
                    "field": field
                }
            }
        else:
            return {
                "bool":
                    {
                        "must_not":
                            {
                                "exists": {
                                    "field": field
                                }
                            }
                    }
            }

    filters = {
        "material_name": create_terms_query,
        "structure_type": create_terms_query,
        "space_group_number": create_terms_query,
        "system_type": create_terms_query,
        "crystal_system": create_terms_query,
        "band_gap": create_range_query,
        "band_gap_direct": create_bool_query,
        "has_band_structure": create_bool_query,
        "has_dos": create_bool_query,
        "has_fermi_surface": create_bool_query,
        "has_thermal_properties": create_bool_query,
        "functional_type": create_terms_query,
        "basis_set_type": create_terms_query,
        "code_name": create_terms_query,
        "mass_density": create_range_query
    }

    for key, value in json_data.items():
        if key != 'search_by':
            filter = filters.get(key)
            if filter is None:
                raise InvalidEncRequest("Unknown property %s" % key, 400)
            try:
                filter_queries.append(filter(key, value))
            except AssertionError as e:
                LOGGER.error("Could not parse query for %s" % key, e)
                raise InvalidEncRequest("Could not parse query for %s" % key, 400)

    # build and execute the search request
    if len(match_query) > 0:
        match_query = dict(match=match_query)
    else:
        match_query = dict(match_all={})
    es_query = {
        'bool': {
            'must': [match_query],
            'filter': filter_queries
        }
    }
    es_request_body = {
        'size': per_page,
        'from': (page - 1) * per_page,
        'query': es_query
    }
    if scroll:
        scroll_id = search_by.get('scroll_id')
        es_request_body = { 
            'size': per_page,
            'query': es_query
        }
        if scroll_id is None:
            es_search_results = es.search(index = es_config['index'], doc_type = 'material', scroll = '1m', body = es_request_body)
        else:
            es_request_body['scroll_id'] = str(scroll_id)
            try:
                es_search_results = es.scroll(scroll_id = scroll_id, scroll = '1m')
            except TransportError as e:
                raise InvalidEncRequest("Scrolling error: %s" % str(e), 510)
            materials_found_list = [render_es_result(hit['_source']) for hit in es_search_results['hits']['hits']]
            return materials_found_list, -1, { 'es_query': es_query, 'scroll_id': scroll_id }
        scroll_id = es_search_results['_scroll_id']
    else:
        es_search_results = es.search(index=es_config['index'], doc_type='material', body=es_request_body)
    total_es_search_results = es_search_results['hits']['total']
    materials_found_list = [render_es_result(hit['_source']) for hit in es_search_results['hits']['hits']]
    if scroll:
        return materials_found_list, total_es_search_results, { 'es_query': es_query, 'scroll_id': scroll_id }
    return materials_found_list, total_es_search_results, { 'es_query': es_query }


@api.route('/esmaterials', methods=['POST'])
@http_auth.login_required
@json
@paginate_queryparams
def get_es_materials(page, per_page):
    # get the content of the POSTed body
    json_data = request.get_json()
    if not json_data:
        raise InvalidEncRequest("No input data provided", 400)

    query = {
        "size": per_page,
        "from": (page - 1) * per_page,
        "query": json_data
    }

    # checking the validity of the syntax of the elastic search POSTed query
    is_query_valid = es.indices.validate_query(index=es_config['index'], doc_type='search', body=dict(query=json_data), explain=True)
    if is_query_valid['valid'] is False:
        if 'error' in is_query_valid:
            raise InvalidEncRequest("Query syntax is not valid.", 400, more=dict(reason=is_query_valid['error']))
        else:
            raise InvalidEncRequest("Query syntax is not valid.", 400)

    # if the POSTed query syntax is valid we perform the query
    es_search_results = es.search(index=es_config['index'], doc_type='material', body=query)
    total_es_search_results = es_search_results['hits']['total']
    materials_found_list = [render_es_result(hit['_source']) for hit in es_search_results['hits']['hits']]

    return materials_found_list, total_es_search_results


@api.route('/materials/<int:material_id>', methods=['GET'])
@http_auth.login_required
@json
def get_material(material_id):
    return dmss.material.match(id=material_id).result()


@api.route('/materials/<int:material_id>/groups', methods=['GET'])
@http_auth.login_required
@json
def get_calcs_groups(material_id):
    material = dmss.material.match(id=material_id).project("material_hash").result()
    if isinstance(material, tuple):
        return material

    def pipeline(hash_key, group_type, minsize):
        return [
            {"$match": {"material_id": str(material_id)}},
            {"$lookup": {
                "from": "energys",
                "localField": "_id",
                "foreignField": "calc_id",
                "as": "energy"
            }},
            {"$unwind": "$energy"},
            {"$match": {"energy.e_kind": "Total E"}},
            {"$sort": {"energy.e_val": 1}},
            {"$group": {
                "_id": {
                    "method_hash": "$method_hash",
                    "group_eos_hash": "$%s" % hash_key
                },
                "calculations_list": {"$push": "$_id"},
                "minimum": {"$first": {"energy": "$energy.e_val", "calc_id": "$_id"}}
            }},
            {"$addFields": {"nr_of_calculations": {"$size": "$calculations_list"}}},
            {"$match": {"nr_of_calculations": {"$gt": minsize}}},
            {"$project": {
                "_id": False,
                "nr_of_calculations": True,
                "calculations_list": True,
                "energy_minimum": "$minimum.energy",
                "representative_calculation_id": "$minimum.calc_id",
                "method_hash": "$_id.method_hash",
                "group_eos_hash": "$_id.group_eos_hash",
                "group_type": group_type,
                "material_hash": material["material_hash"]
            }}
        ]

    eos_groups = list(dmss.calc.collection.aggregate(pipeline("group_eos_hash", "equation of state", 4)))
    convergence_groups = list(dmss.calc.collection.aggregate(pipeline("group_parametervariation_hash", "parameter variation", 2)))
    groups = eos_groups + convergence_groups
    for group in groups:
        group["calculations_list"] = [int(calc) for calc in group["calculations_list"]]
        group["representative_calculation_id"] = int(group["representative_calculation_id"])
    return dict(groups=groups, total_groups=len(groups))


# Element/Position
@api.route('/materials/<int:material_id>/elements', methods=['GET'])
@http_auth.login_required
@json
@paginate_queryparams
def get_elements(material_id, *args, **kwargs):
    return dmss.element.match(parent_id=material_id).paginate(*args, **kwargs).results()


@api.route('/materials/<int:material_id>/elements/<int:element_id>', methods=['GET'])
@http_auth.login_required
@json
def get_element(material_id, element_id):
    return dmss.element.match(parent_id=material_id, id=element_id).result()


# Cell
@api.route('/materials/<int:material_id>/cells', methods=['GET'])
@http_auth.login_required
@json
@paginate_queryparams
def get_cells(material_id, *args, **kwargs):
    return dmss.cell.match(parent_id=material_id).paginate(*args, **kwargs).results()


@api.route('/materials/<int:material_id>/cells/<int:cell_id>', methods=['GET'])
@http_auth.login_required
@json
def get_cell(material_id, cell_id):
    return dmss.cell.match(parent_id=material_id, id=cell_id).result()


# Calc
@api.route('/materials/<int:material_id>/calculations', methods=['GET'])
@http_auth.login_required
@json
@paginate_queryparams
def get_calcs(material_id, *args, **kwargs):
    query = dmss.calc.match(parent_id=material_id).paginate(*args, **kwargs)
    property = request.args.get("property", None)
    if property is None:
        query = query.join("energy").exclude(["brillouin_zone_json", "settings_basis_set_json"])
    else:
        if property == "energy":
            query = query.join("energy")
        # TODO in principle we could also join diagram data here ...
        # but its complex, since the joined data has to be filtered for the right diagram data type.

        query = query.project(property)

    return query.results()


@api.route('/materials/<int:material_id>/calculations/<int:calc_id>', methods=['GET'])
@http_auth.login_required
@json
def get_calc(material_id, calc_id):
    property = request.args.get("property", None)
    if property in diagram_data_types:
        result = dmss.diagram_data.match(parent_id=calc_id, query=dict(data_type=property)).result()
        if result is None:
            return {}
        elif isinstance(result, dict):
            return {property: result["data_content"]}
        else:
            return result
    else:
        query = dmss.calc.match(parent_id=material_id, id=calc_id)
        if property == "energy":
            query = query.join("energy")
        return query.project(property).result()


# Energy_decomposition
@api.route('/materials/<int:material_id>/calculations/<int:calc_id>/energies', methods=['GET'])
@http_auth.login_required
@json
@paginate_queryparams
def get_energies(material_id, calc_id, *args, **kwargs):
    return dmss.energy.match(parent_id=calc_id).paginate(*args, **kwargs).results()


@api.route('/materials/<int:material_id>/calculations/<int:calc_id>/energies/<int:energy_id>', methods=['GET'])
@http_auth.login_required
@json
def get_energy(material_id, calc_id, energy_id):
    return dmss.energy.match(parent_id=calc_id, id=energy_id).result()


# Author
@api.route('/materials/<int:material_id>/calculations/<int:calc_id>/authors', methods=['GET'])
@http_auth.login_required
@json
@paginate_queryparams
def get_authors(material_id, calc_id, *args, **kwargs):
    return dmss.contributor.match(parent_id=calc_id).paginate(*args, **kwargs).results()


@api.route('/materials/<int:material_id>/calculations/<int:calc_id>/authors/<int:author_id>', methods=['GET'])
@http_auth.login_required
@json
def get_author(material_id, calc_id, author_id):
    return dmss.contributor.match(parent_id=calc_id, id=author_id).result()


# Route to make data available for implementing GUI suggestions for some text fields
@api.route('/suggestions', methods=['GET'])
@http_auth.login_required
def get_suggestions():
    suggestion_properties = ['material_name',
                             'structure_type', 'system_type',
                             'crystal_system', 'space_group_number',
                             'space_group_international_short_symbol',
                             'strukturbericht_designation',
                             'code_name'
                             ]
    # define sorting order,
    # default is document count descending
    # different ones for particular properties can be defined here
    default_order = {'_count': 'desc'}
    properties_orders = {
        'material_name': {"_key": "asc"},
        'strukturbericht_designation': {"_key": "asc"}
    }

    property = request.args.get('property')
    if property not in suggestion_properties:
        return {"message": "Quantity not provided or not available."}, 400

    order = properties_orders.get(property, default_order)
    search_results = es.search(index=es_config['index'], doc_type='material', body={
        "size": 1,
        "aggs": {
            "suggestions": {
                "terms": {
                    "field": property,
                    "size": 10000,
                    "order": order
                }
            }
        }
    })
    suggestions = [bucket['key'] for bucket in search_results['aggregations']['suggestions']['buckets']]
    return jsonify(**{property: suggestions})


@api.route('/flagme', methods=['POST'])
@http_auth.login_required
def flagme():
    # get the content of the POSTed body
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400
    # collect the POSTed data
    title = json_data['title']
    description = json_data['description']
    label = "WebGUI flag"
    # needed for RQ Scheduler
    from rq_scheduler import Scheduler
    # Get a scheduler for the "default" queue
    scheduler = Scheduler(connection=Redis())

    from datetime import timedelta
    scheduler.enqueue_in(timedelta(minutes=3), flag_to_gitlab, title, description, label)
    return jsonify('task scheduled - the issue will be added for review')


@api.route('/ping', methods=['GET'])
def ping():
    """ Just a simple route for checking the API service status
    Can be used within our monitoring machine, or to get notifications
    within Slack, ...
    """
    return jsonify(data="Pong!")


@api.route('/newtoken/', methods=['GET'])
def get_token():
    """ A simple route to generate a new token, for development or if somebody needs a token
    with a validity period different from the standard one for anonymous users: 24 hours
    """
    # let's generate a new token, with a random/sample user
    user_for_token = "sampleuser"
    email_for_token = "email@example.com"
    # token with 30 days validity period: 2592000
    # or for a token valid 5 years: 157680000
    expires_in = 2592000
    new_token = generate_auth_token(user_for_token, expires_in)
    # to have the token printed to the terminal, as well - for development
    # print(new_token)
    # let's verify the generated token is valid and get the username from it to prove it
    username = verify_auth_token(new_token)
    # print(username)
    return jsonify({"status": "Token Created", "user": {"username": username, "email": email_for_token},
                    "token": {"data": new_token.decode('ascii'), "expires_in": expires_in,
                              "user_from_token": user_for_token}})


######################
# SAML specific routes
######################

@api.route('/saml/', methods=['GET', 'POST'])
def index():
    req = prepare_flask_request(request)
    auth = init_saml_auth(req)
    errors = []
    not_auth_warn = False
    success_slo = False
    attributes = False
    paint_logout = False

    # SSO action (SP-SSO initiated). Will send an AuthNRequest to the IdP
    # initiate the login for API and redirect to user details if login is successful
    if 'sso' in request.args:
        return_to = url_for('api.user_attrs')
        return redirect(auth.login(return_to))
        # return redirect(auth.login())
    # another SSO init action with a custom RelayState URL
    # initiate the login for GUI and redirect to user back to GUI if successful
    elif 'sso2' in request.args:
        # based on machine's hostname use different base URL for redirection
        # TODO switch to use environment variables: PRODUCTION, TESTING, DEVELOPMENT
        if 'production' in hostname:
            return_to = 'https://encyclopedia.nomad-coe.eu/gui/'
        else:
            return_to = '%sgui/' % request.host_url
        # return_to = '%sgui/' % request.host_url
        return redirect(auth.login(return_to))
        # return_to = url_for('api.user_attrs')
        # return redirect(auth.login(return_to))
    elif 'slo' in request.args:
        name_id = None
        session_index = None
        if 'samlNameId' in session:
            name_id = session['samlNameId']
        if 'samlSessionIndex' in session:
            session_index = session['samlSessionIndex']

        return redirect(auth.logout(name_id=name_id, session_index=session_index))
    # Assertion Consumer Service
    # handles the SAML response that the IdP forwards to the SP through the user's client
    elif 'acs' in request.args:
        # Process the Response of the IdP
        auth.process_response()
        # This method receives an array with the errors
        errors = auth.get_errors()
        not_auth_warn = not auth.is_authenticated()
        if len(errors) == 0:
            # Retrieves user data
            session['samlUserdata'] = auth.get_attributes()
            session['samlNameId'] = auth.get_nameid()
            session['samlSessionIndex'] = auth.get_session_index()
            self_url = OneLogin_Saml2_Utils.get_self_url(req)
            # user is authenticated, let's generate an API access token
            # first get the username received from IdP server
            username = session['samlUserdata']["urn:oid:0.9.2342.19200300.100.1.1"][0]
            # get the user email received from IdP server
            user_email = session['samlUserdata']["urn:oid:0.9.2342.19200300.100.1.3"][0]
            # having the username, generate the token having the username encoded in it
            # save the token to the session to access it in a different view: /saml/user
            # token expiration time set to 24h
            session['token_expires_in'] = 86400
            session['token'] = generate_auth_token(username, session['token_expires_in'])
            token = session['token']
            # Redirect if there is a RelayState
            if 'RelayState' in request.form and self_url != request.form['RelayState']:
                import json
                redirect_to_relaystate = redirect(auth.redirect_to(request.form['RelayState']))
                response = make_response(redirect_to_relaystate)
                cookie_data = {"status": "Authenticated",
                               "user": {"username": username, "email": user_email},
                               "token": {"data": token.decode('ascii'),
                                         "expires_in": session['token_expires_in']}}
                # production and staging machines have different domain names
                # and the cookie should be set on the corresponding domain name
                if 'production' in hostname:
                    response.set_cookie('user_info', json.dumps(cookie_data), domain='.nomad-coe.eu')
                else:
                    response.set_cookie('user_info', json.dumps(cookie_data), domain='.mpg.de')
                return response
            # # user is authenticated, let's generate an API access token
            # # first get the username received from IdP server
            # username = session['samlUserdata']["urn:oid:0.9.2342.19200300.100.1.1"][0]
            # # having the username, generate the token having the username encoded in it
            # # save the token to the session to access it in a different view: /saml/user
            # # token expiration time set to 24h
            # session['token_expires_in'] = 86400
            # session['token'] = generate_auth_token(username, session['token_expires_in'])
            # return redirect(url_for('api.user_attrs'))
    # Single Logout Service (not used right now. /saml/logout endpoint is used)
    elif 'sls' in request.args:
        dscb = lambda: session.clear()
        url = auth.process_slo(delete_session_cb=dscb)
        errors = auth.get_errors()
        if len(errors) == 0:
            if url is not None:
                return redirect(url)
            else:
                success_slo = True
        # delete_session_callback = lambda: request.session.flush()
        # url = auth.process_slo(delete_session_cb=delete_session_callback)
        # errors = auth.get_errors()
        # if len(errors) == 0:
        #     if url is not None:
        #         print(url)
        #         return redirect(url)
        #     else:
        #         print("Sucessfully Logged out")
        # else:
        #     print("Error when processing SLO: %s" % (', '.join(errors)))

    if 'samlUserdata' in session:
        paint_logout = True
        if len(session['samlUserdata']) > 0:
            attributes = session['samlUserdata'].items()

    # return render_template(
    #     'index.html',
    #     errors=errors,
    #     not_auth_warn=not_auth_warn,
    #     success_slo=success_slo,
    #     attributes=attributes,
    #     paint_logout=paint_logout
    # )
    if 'samlUserdata' in session:
        # Added 'api' string as a quick fix for redirection to /saml/user/ after the user is authenticated.
        # Before the fix the url was domain.com/v1.0/saml/user/ which doesn't exists (because should be
        # domain.com/api/v1.0/saml/user/) and nginx redirected to the /gui and not showing the user's details, token ...
        if 'production' in hostname:
            return redirect('api' + url_for('api.user_attrs'))
        else:
            return redirect(url_for('api.user_attrs'))
    else:
        return redirect(auth.login())


@api.route('/saml/user/')
def user_attrs():
    paint_logout = False
    attributes = False
    if 'samlUserdata' in session:
        paint_logout = True
        if len(session['samlUserdata']) > 0:
            # attributes = session['samlUserdata'].items()
            attributes = session['samlUserdata']
            # let's take only the attributes we need. Each attribute is an array
            # by default in IdP, so, we take the first item of the array
            user_email = attributes["urn:oid:0.9.2342.19200300.100.1.3"][0]
            user_login = attributes["urn:oid:0.9.2342.19200300.100.1.1"][0]
            token = session['token']
            expires_in = session['token_expires_in']
            # next line is added for debug only, to be removed soon
            user_from_token = verify_auth_token(token)
            # if needed to show also all available user attributes, uncomment next line
            # return jsonify(user_data=attributes, username=user_login, email=user_email)
            # return jsonify(username=user_login, email=user_email,
            #                token=token.decode('ascii'), expires_in=expires_in,
            #                user_from_token=user_from_token)
            return jsonify({"status": "Authenticated",
                            "user": {"username": user_login, "email": user_email},
                            "token": {"data": token.decode('ascii'),
                                      "expires_in": expires_in}})
    else:
        return jsonify({"status": "Unauthenticated"})
    # return render_template('attrs.html', paint_logout=paint_logout,
    #                        attributes=attributes)


@api.route('/saml/logout/')
def logout():
    # the single logout is not working right now
    # just clear the session - not a final solution
    if 'samlUserdata' in session:
        session.clear()
        # idp_logout_url = 'https://nomad-login.csc.fi/idp/profile/Logout'
        idp_logout_url = 'https://idp.nomad-coe.eu/idp/profile/Logout'
        return redirect(idp_logout_url)

    return jsonify({"status": "Unauthenticated"})
    # if 'samlUserdata' in session:
    #     paint_logout = True
    #     success_slo = True
    #     if len(session['samlUserdata']) > 0:
    #         attributes = session['samlUserdata'].items()
    #
    # return render_template(
    #     'index.html',
    #     # errors=errors,
    #     # not_auth_warn=not_auth_warn,
    #     success_slo=success_slo,
    #     attributes=attributes,
    #     paint_logout=paint_logout
    # )


@api.route('/saml/metadata/')
def metadata():
    req = prepare_flask_request(request)
    auth = init_saml_auth(req)
    settings = auth.get_settings()
    metadata = settings.get_sp_metadata()
    errors = settings.validate_metadata(metadata)

    if len(errors) == 0:
        resp = make_response(metadata, 200)
        resp.headers['Content-Type'] = 'text/xml'
    else:
        resp = make_response(', '.join(errors), 500)
    return resp


#############################
# END OF SAML specific routes
#############################
