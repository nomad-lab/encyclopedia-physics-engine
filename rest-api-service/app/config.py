# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

SECRET_KEY = 'InVzZXIxQGdtlsLmNvbSI.YH6iB4wE5dDMNjpf-cX2Q9MIjyY'

dmss_config = dict(host=os.environ.get('ENC_DMSS_HOST', 'localhost'),
                   port=int(os.environ.get('ENC_DMSS_PORT', '27017')),
                   db=os.environ.get('ENC_DMSS_DB', 'nd_1_11_6_patch_3'))

es_config = dict(host=os.environ.get('ENC_ES_HOST', 'localhost'),
                 port=int(os.environ.get('ENC_ES_PORT', '9200')),
                 index=os.environ.get('ENC_ES_INDEX', 'nd_1_11_6_patch_3'))
