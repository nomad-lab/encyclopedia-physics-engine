# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask, request, jsonify, make_response
# needed for Decorator to accept cross-domain AJAX requests
from datetime import timedelta
from flask import current_app
from functools import update_wrapper

app = Flask(__name__)

# Decorator to accept cross-domain AJAX requests
# -*- coding: utf-8 -*-
def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Credentials'] = 'true'
            h['Access-Control-Allow-Headers'] = \
                "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

# Model (right now just JSON, not database queries)
Material = {
  "id": 1,
  "material_name": "None",
  "formula": "Mg3Rb3",
  "space_group": 6,
  "point_group": "m",
  "bravais_lattice": "mP",
  "crystal_system": "Monoclinic",
  "structure_type": "rock salt",
  "system_type": "bulk",
  "mat_hash": "867f4a2b8e13b9a3491a17f2fbf96dba118144b5989d9158fafb3ebd76c7a1c83fa9a5b638f607bc24afc932b35d489d448ce297ca6646f78dd0b4000e4382e2",
  "nr_of_calculations": 50
}

Element = {
  "id": 1,
  "label": 37,
  # "position": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  "position": [0.64576208,0,0.29152447],
  "wyckoff": "a"
}

Cell = {
  "id": 1,
  "is_primitive": "f",
  # "a": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  # "b": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  # "c": {"x": 3.1415, "y": 3.1415, "z": 3.1415}
  "a": [3.943829e-10,0,0],
  "b": [0,6.753777e-10,0],
  "c": [-1.971914e-10,0,1.065986e-09]
}

Calculation = {
  "id": 1,
  "code_name": "vasp",
  "code_version": "None",
  "basis_set_type": "plane waves",
  "basis_set_quality_quantifier": "None",
  "core_electron_treatment": "pseudopotential",
  "pseudopotential_description": "None",
  "functional_type": "GGA",
  "functional_long_name": "GGA_C_PBE+GGA_X_PBE",
  "band_gap": 0.1415,
  "band_gap_direct": "t",
  # "band_gap_lower": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  # "band_gap_upper": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  "band_gap_lower": [3.1415, 3.1415, 3.1415],
  "band_gap_upper": [3.1415, 3.1415, 3.1415],
  "cell_volume": 2.83933276368315e-28,
  "density": 3.1415,
  "k_point_grid_description": "None",
  # "lattice_parameters": {"a": 3.1415, "b": 3.1415, "c": 3.1415, "alpha": 3.1415, "beta": 3.1415, "gamma": 3.1415},
  "lattice_parameters": [3.943829e-10,6.753777e-10,1.08407130781603e-09,2.09439495599987,2.09439495599987,1.5707963267949],
  "pressure": 3.1415,
  # "smearing": {"smearing_kind": "TEXT", "smearing_parameter": 3.1415},
  "smearing": ["fermi", 0.01],
  "fermi_surface": "None",
  "calculation_repository_id": "None",
  "repository_uri": "nmd://R0pyjkhVZ1nqM5qfB7h7rmoysmMnh/data/Mg/Rb/static/vasprun.xml"
}

Energy = {
  "id": 1,
  "e_val": -8.835275517913e-19,
  "e_kind": "_energy_total"
}

Author = {
  "id": 1,
  "first_name": "Sheldon",
  "last_name": "Cooper"
}

Band = {
  "id": 1,
  # "kpt": {"x": 3.1415, "y": 3.1415, "z": 3.1415},
  "kpt": [3.1415, 3.1415, 3.1415],
  "e_kpt": 3.1415,
  "segment": 10
}

DOS = {
  "id": 1,
  "e_dos": 1.1415,
  "dos": 1.1415
}

Pages = {
  "first_url": "https://api.nomad-enc.eu/v1.0/.../collection?per_page=25&page=1",
  "last_url": "https://api.nomad-enc.eu/v1.0/.../collection?per_page=25&page=4",
  "next_url": "https://api.nomad-enc.eu/v1.0/.../collection?per_page=25&page=2",
  "prev_url": "null",
  "page": 1,
  "per_page": 25,
  "total": 100
}

# Routes - URI
# Material
@app.route('/v1.0/materials', methods=['GET'])
@crossdomain(origin='*')
def get_materials():
    if request.args.get('formula'):
        user_formula = request.args['formula']
        return jsonify(formula=user_formula, total_results = 100, results = [Material, Material, Material], pages=Pages)
    elif request.args.get('element'):
        user_element = request.args['element']
        return jsonify(element=user_element, total_results = 100, results = [Material, Material, Material], pages=Pages)
    else:
        return jsonify(total_results = 100, results = [Material, Material, Material], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>', methods=['GET'])
@crossdomain(origin='*')
def get_material(material_id):
    #return request.path
    return jsonify(Material)

# Element/Position
@app.route('/v1.0/materials/<int:material_id>/elements', methods=['GET'])
@crossdomain(origin='*')
def get_elements(material_id):
    return jsonify(total_results = 100, results = [Element, Element, Element], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/elements/<int:element_id>', methods=['GET'])
@crossdomain(origin='*')
def get_element(material_id, element_id):
    return jsonify(Element)

# Cell
@app.route('/v1.0/materials/<int:material_id>/cells', methods=['GET'])
@crossdomain(origin='*')
def get_cells(material_id):
    return jsonify(total_results = 100, results = [Cell, Cell, Cell], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/cells/<int:cell_id>', methods=['GET'])
@crossdomain(origin='*')
def get_cell(material_id, cell_id):
    return jsonify(Cell)

# Calc
@app.route('/v1.0/materials/<int:material_id>/calculations', methods=['GET'])
@crossdomain(origin='*')
def get_calcs(material_id):
    return jsonify(total_results = 100, results = [Calculation, Calculation, Calculation], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>', methods=['GET'])
@crossdomain(origin='*')
def get_calc(material_id, calc_id):
    return jsonify(Calculation)

# Energy_decomposition
@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/energies', methods=['GET'])
@crossdomain(origin='*')
def get_energies(material_id, calc_id):
    return jsonify(total_results = 100, results = [Energy, Energy, Energy], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/energies/<int:energy_id>', methods=['GET'])
@crossdomain(origin='*')
def get_energy(material_id, calc_id, energy_id):
    return jsonify(Energy)

# Author
@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/authors', methods=['GET'])
@crossdomain(origin='*')
def get_authors(material_id, calc_id):
    return jsonify(total_results = 100, results = [Author, Author, Author], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/authors/<int:author_id>', methods=['GET'])
@crossdomain(origin='*')
def get_author(material_id, calc_id, author_id):
    return jsonify(Author)

# Band Structure
@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/bands', methods=['GET'])
@crossdomain(origin='*')
def get_bands(material_id, calc_id):
    return jsonify(total_results = 100, results = [Band, Band, Band], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/bands/<int:band_id>', methods=['GET'])
@crossdomain(origin='*')
def get_band(material_id, calc_id, band_id):
    return jsonify(Band)

# Density of state
@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/density_of_states', methods=['GET'])
@crossdomain(origin='*')
def get_density_of_states(material_id, calc_id):
    return jsonify(total_results = 100, results = [DOS, DOS, DOS], pages=Pages)

@app.route('/v1.0/materials/<int:material_id>/calculations/<int:calc_id>/density_of_states/<int:dos_id>', methods=['GET'])
@crossdomain(origin='*')
def get_density_of_state(material_id, calc_id, dos_id):
    return jsonify(DOS)

# little error handler for "404: Not Found" page
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Resource not found'}), 404)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
