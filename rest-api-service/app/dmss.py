# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from pymongo import MongoClient

from app.config import dmss_config

_default_per_page = 25


# transforms
def json_str(value):
    if value is None or value == '':
        return None
    else:
        return json.loads(value)


def int_id(value):
    return int(value)


def vector(value):
    return "(%s)" % ", ".join([json.dumps(item).strip('"') for item in value])


diagram_data_types = ['dos', 'band_structure', 'phonon_dos', 'phonon_dispersion', 'specific_heat_cv',
                      'helmholtz_free_energy', 'qha_bulk_modulus', 'qha_helmholtz_free_energy', 'qha_mass_density',
                      'qha_specific_heat_cv', 'qha_thermal_expansion', 'gw_spectral_function', 'elastic_toolbox_data']

model = {
    value["key"]: value for value in [
        {
            "key": "material",
            "collection": "material",
            "label": "material",
            "parent": None,
            "parent_key": None,
            "transforms": [],
            "defaults": [
                ("periodicity", None)
            ]
        },
        {
            "key": "calc",
            "collection": "calc",
            "label": "calculation",
            "parent": "material",
            "parent_key": "material_id",
            "transforms": [
                ("brillouin_zone_json", json_str),
                ("lattice_parameters", vector),
                ("settings_basis_set_json", json_str),
                ("wyckoff_groups_json", json_str),
                ("smearing", vector),
                ("material_id", ("material", int_id))
            ],
            "defaults": [
                ("energy", [])
            ],
            "aggregate":  [
                {
                    "$lookup": {
                        "from": "energys",
                        "localField": "_id",
                        "foreignField": "calc_id",
                        "as": "energy"
                    }
                }
            ]
        },
        {
            "key": "element",
            "collection": "elements",
            "label": "element",
            "parent": "material",
            "parent_key": "material_id",
            "transforms": [
                ("position", vector)
            ]
        },
        {
            "key": "cell",
            "collection": "cells",
            "label": "cell",
            "parent": "material",
            "parent_key": "material_id",
            "transforms": [(i, vector) for i in ["a", "b", "c"]]
        },
        {
            "key": "contributor",
            "collection": "contributors",
            "label": "contributor",
            "parent": "calc",
            "parent_key": "calc_id",
            "transforms": []
        },
        {
            "key": "energy",
            "collection": "energys",
            "label": "energy",
            "parent": "calc",
            "parent_key": "calc_id",
            "transforms": [
                ("calc_id", ("calc", int_id))
            ]
        },
        {
            "key": "diagram_data",
            "collection": "diagram_datas",
            "label": "diagram data",
            "parent": "calc",
            "parent_key": "calc_id",
            "transforms": [("data_content", json_str)]
        }
    ]
}


class DMSS:
    """
    This class provides an abstraction for accessing data in the mongo dmss without having to use the
    mongo client directly. Allows to create complex aggregations that are executed right within the
    mongodb server. Those aggregation allow to match, join, paginate, project (and exclude single properties).
    It is basically a SQLAlchemy/SQL replacement.
    """
    def __init__(self):
        super().__init__()
        self._mongo_client = MongoClient(dmss_config['host'], dmss_config['port'])
        self._mongo_dmss = self._mongo_client[dmss_config['db']]

    def __getattribute__(self, name):
        if name in model:
            return Aggregation(self._mongo_dmss[model[name]["collection"]], model[name])
        else:
            return super().__getattribute__(name)


class Aggregation:

    def __init__(self, mongo_collection, model):
        self.collection = mongo_collection
        self._model = model
        self._match_stages = []
        self._aggregation_stages = []
        self._results = None

    def match(self, parent_id=None, id=None, query=None):
        assert self._results is None, "Cannot alter aggregation, once it was executed."
        if query is None:
            query = {}
        if parent_id is not None:
            query[self._model["parent_key"]] = str(parent_id)
        if id is not None:
            query["_id"] = str(id)

        self._match_stages.append({"$match": query})
        return self

    def paginate(self, page, per_page):
        assert self._results is None, "Cannot alter aggregation, once it was executed."
        self._aggregation_stages.append({"$skip": (page - 1) * per_page})
        self._aggregation_stages.append({"$limit": per_page})
        return self

    def join(self, collection_model):
        assert self._results is None, "Cannot alter aggregation, once it was executed."

        if isinstance(collection_model, str):
            collection_model = model[collection_model]
        lookup = {
            "from": collection_model["collection"],
            "localField": "_id",
            "foreignField": collection_model["parent_key"],
            "as": collection_model["key"]
        }
        self._aggregation_stages.append({"$lookup": lookup})
        return self

    def exclude(self, keys):
        assert self._results is None, "Cannot alter aggregation, once it was executed."
        if isinstance(keys, str):
            keys = [keys]

        self._aggregation_stages.append({"$project": {key: 0 for key in keys}})
        return self

    def project(self, keys):
        assert self._results is None, "Cannot alter aggregation, once it was executed."

        if keys is None:
            return self

        if isinstance(keys, str):
            keys = [keys]

        self._aggregation_stages.append({"$project": {key: 1 for key in keys}})
        return self

    def _execute(self):
        if self._results is None:
            assert len(self._match_stages) > 0, "Matching must not be empty."

            pipeline = []
            for match in self._match_stages:
                pipeline.append(match)

            if len(self._aggregation_stages) == 0:
                self._aggregation_stages.append({
                    "$project": {
                        "does_not_exist": 0
                    }
                })

            pipeline.append({
                "$facet": {
                    "count": [
                        {
                            "$count": "total"
                        }
                    ],
                    "results": self._aggregation_stages
                }
            })

            self._results = next(self.collection.aggregate(pipeline))

        if len(self._results["count"]) > 0:
            return self._results["results"], self._results["count"][0]["total"]
        else:
            return [], 0

    def _transform(self, item):
        if "_id" in item:
            item["id"] = int(item["_id"])
            del(item["_id"])

        parent_key = self._model["parent_key"]
        if parent_key is not None and parent_key in item:
            item[parent_key] = int_id(item[parent_key])

        for key, transform in self._model["transforms"]:
            if key in item:
                if isinstance(transform, tuple):
                    new_key, transform = transform
                    item[new_key] = transform(item[key])
                    del(item[key])
                else:
                    item[key] = transform(item[key])

        for key, default in self._model.get("defaults", []):
            if not key in item:
                item[key] = default

        return item

    def result(self):
        results, count = self.results()
        assert 0 <= count <= 1, "Cannot look for a single result in a list of results."
        return results[0] if len(results) == 1 else None

    def results(self):
        results, count = self._execute()

        return [self._transform(result) for result in results], count
