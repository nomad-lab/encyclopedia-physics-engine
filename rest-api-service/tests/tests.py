# Copyright 2016-2018 Ioan Vancea, Markus Scheidgen
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import unittest
import sys

from app import app
from app.utils import get_atom_number

material_keys = [
    "cell_angles_string",
    "crystal_system",
    "formula",
    "formula_reduced",
    "has_free_wyckoff_parameters",
    "id",
    "material_hash",
    "material_name",
    # "periodicity",
    "point_group",
    "space_group_international_short_symbol",
    "space_group_number",
    # "structure_prototype",
    # "structure_type",
    # "strukturbericht_designation",
    "system_type"
]

calc_keys = [
    "atomic_density",
    "band_gap",
    # "band_gap_direct",
    # "basis_set_short_name",
    "basis_set_type",
    # "brillouin_zone_json",
    # "calculation_pid",
    "cell_volume",
    "code_name",
    "code_version",
    "core_electron_treatment",
    # "energy",
    # "fermi_surface",
    "functional_long_name",
    "functional_type",
    "group_eos_hash",
    "group_parametervariation_hash",
    "gw_type",
    "has_band_structure",
    "has_dos",
    "has_fermi_surface",
    "has_thermal_properties",
    "id",
    # "is_curtarolo_path",
    # "k_point_grid_description",
    "lattice_parameters",
    "mainfile_uri",
    "mass_density",
    # "material",
    "method_hash",
    "pressure",
    # "pseudopotential_type",
    "repository_upload_comment",
    "run_type",
    "scf_threshold",
    "smearing",
    "wyckoff_groups_json"
]

calc_properties = [
    'run_type',
    'code_name',
    'code_version',
    'basis_set_type',
    'mass_density',
    'atomic_density',
    'basis_set_short_name',
    'core_electron_treatment',
    'pseudopotential_type',
    'functional_type',
    'functional_long_name',
    'band_gap',
    'band_gap_direct',
    'cell_volume',
    'k_point_grid_description',
    'lattice_parameters',
    'pressure',
    'smearing',
    'fermi_surface',
    'calculation_pid',
    'gw_type',
    'repository_download_uri',
    'is_curtarolo_path',
    'brillouin_zone_json',
    'mainfile_uri',
    'repository_upload_comment',
    'method_hash',
    'group_eos_hash',
    'src_filepath',
    'material_id',
    # 'band_structure',
    # 'phonon_dos',
    # 'phonon_dispersion',
    # 'specific_heat_cv',
    # 'qha_bulk_modulus',
    # 'qha_helmholtz_free_energy',
    # 'qha_mass_density',
    # 'qha_specific_heat_cv',
    # 'qha_thermal_expansion'
]

suggestion_properties = ['material_name', 'structure_type', 'space_group_number', 'code_name']

material_id = 1


class ApiTest(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def tearDown(self):
        pass
        # os.close(self.db_fd)
        # os.unlink(flaskr.app.config['DATABASE'])

    def get(self, path):
        response = self.app.get('/v1.0%s' % path)
        assert response.status_code == 200

        return json.loads(response.get_data(as_text=True))

    def get_first(self, path):
        data = self.get(path)
        assert "pages" in data
        assert "results" in data

        assert data["pages"] is None or data["pages"]["total"] >= 1
        return data["results"][0]

    def post(self, path, body):
        data = json.dumps(body)
        response = self.app.post('/v1.0%s' % path, data=data, content_type='application/json')
        assert response.status_code == 200
        return json.loads(response.get_data(as_text=True))

    def test_material_keys(self):
        data = self.get('/materials/%s' % material_id)

        for key in material_keys:
            assert key in data
            self.get('/materials/%s?property=%s' % (material_id, key))

        assert data["id"] == material_id

    def test_elements(self):
        data = self.get_first('/materials/%s/elements' % material_id)
        data = self.get('/materials/%s/elements/%s' % (material_id, data["id"]))
        assert "label" in data
        assert "material_id" in data
        assert material_id == data["material_id"]
        assert "position" in data
        assert "wyckoff" in data

    def test_cells(self):
        data = self.get_first('/materials/%s/cells' % material_id)
        data = self.get('/materials/%s/cells/%s' % (material_id, data["id"]))

        assert "material_id" in data
        assert material_id == data["material_id"]
        assert "is_primitive" in data

    def test_calc_keys(self):
        data = self.get_first('/materials/%s/calculations' % material_id)

        for key in calc_keys:
            self.assertIn(key, data)

    def test_calc_properties(self):
        calc = self.get_first('/materials/%s/calculations' % material_id)

        for property in calc_properties:
            url = '/materials/%s/calculations/%s?property=%s' % (material_id, calc["id"], property)
            self.get(url)

    def test_energies(self):
        calc_id = self.get_first('/materials/%s/calculations' % material_id)["id"]
        data = self.get_first('/materials/%s/calculations/%s/energies' % (material_id, calc_id))

        assert "e_kind" in data
        assert "e_val" in data
        assert "calc" in data
        assert calc_id == data["calc"]

    def test_groups(self):
        data = self.get("/materials/%s/groups" % material_id)
        assert "groups" in data
        assert "total_groups" in data
        assert data["total_groups"] > 0
        if data["total_groups"] > 0:
            group = data["groups"][0]

            assert "calculations_list" in group
            assert "energy_minimum" in group
            assert "method_hash" in group
            assert "nr_of_calculations" in group
            assert "representative_calculation_id" in group

    def test_suggestions(self):
        for suggestion_property in suggestion_properties:
            data = self.get("/suggestions?property=%s" % suggestion_property)
            assert len(data) > 0

    def test_es_search(self):
        data = self.post("/esmaterials", body=dict(match_all={}))
        assert "total_results" in data
        assert data["total_results"] > 0

    def perform_search_test(self, match, filter, has_results):
        query = dict(filter)
        query['search_by'] = match
        data = self.post("/materials", query)
        self.assertIn('results', data)
        if has_results:
            self.assertTrue(data['results'])
        else:
            self.assertFalse(data['results'])

    def test_search_all(self):
        self.perform_search_test({}, {}, True)

    def test_search_paging(self):
        self.perform_search_test(dict(page=1), {}, True)
        self.perform_search_test(dict(page=2), {}, False)

    def test_formula_search(self):
        self.perform_search_test(dict(formula='Cr', exclusive='0'), {}, True)
        self.perform_search_test(dict(formula='CrBr', exclusive='0'), {}, False)
        self.perform_search_test(dict(formula='CrBr', exclusive='1'), {}, False)
        self.perform_search_test(dict(formula='Cr', exclusive='1'), {}, True)

    def test_elements_search(self):
        def elements(atoms):
            return ",".join(atoms)

        self.perform_search_test(dict(element=elements(['Cr', 'Br']), exclusive='1'), {}, False)
        self.perform_search_test(dict(element=elements(['Cr', 'Br'])), {}, False)
        self.perform_search_test(dict(element=elements(['Cr']), exclusive='1'), {}, True)
        self.perform_search_test(dict(element=elements(['Cr'])), {}, True)

    def test_search_filters(self):
        self.perform_search_test(dict(formula='Cr'), dict(has_dos=False), True)
        self.perform_search_test(dict(formula='Cr'), dict(has_dos=True), False)

        self.perform_search_test(dict(formula='Cr'), dict(functional_type=["GGA"]), True)
        self.perform_search_test(dict(formula='Cr'), dict(functional_type=["GGA", "unknown"]), True)
        self.perform_search_test(dict(formula='Cr'), dict(functional_type=["unknown"]), False)

        self.perform_search_test(dict(formula='Cr'), dict(mass_density=dict(min=1, max=8000)), True)
        self.perform_search_test(dict(formula='Cr'), dict(mass_density=dict(min=1, max=2)), False)


if __name__ == "__main__":
    logging.basicConfig(level=logging.CRITICAL)

    suite = unittest.TestSuite()
    suite.addTest(unittest.defaultTestLoader.loadTestsFromName(__name__))
    result = unittest.TextTestRunner(verbosity=0).run(suite)

    # We need to return a non-zero exit code for the gitlab CI to detect errors
    sys.exit(not result.wasSuccessful())
