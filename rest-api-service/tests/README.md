Currently these are very basic tests that only assert if all endpoints
successfully return something and if this something contains the 
necessary keys. There is not check on the values.

The tests for functionality based on postgres require running data 
services with ingested data from ./data.
The given data belongs to one material and this material needs to 
have the id "1".

To process/ingest data for test use:

```
enc process ./data --set-label api-test --update
enc ingest --mongo --reset-mongo-dmss --label api-test
enc search --mongo-ids --reset-search
```

The test for mongodb data use a mongodb mockup based
on the mongodb export in `.mockup_data.json`. This export is
created from an actual mongo dmss that contains the fully ingested data 
from `.data`. To create a new export use:

```
enc process ./data --set-label api-test --update
enc ingest --mongo --reset-mongo-dmss --label api-test
python export_dmss.py > mockup_data.json
```