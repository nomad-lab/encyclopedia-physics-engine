## Limitations of this version compared to prior SQL-based API
- no `calculations_list_matching_criteria` and `calculations_list_matching_criteria` in search results


## REST API Implementation

This folder structure has two top-level folders:
- The Flask application lives inside a package generically named app.
- [app/api_v1_0/routes.py](app/api_v1_0/routes.py) contains the actual API endpoint
implementations.
- [app/dmss.py](app/dmss.py) contains a dmss abstraction to access mongodb
- Unit tests are written in the [tests](tests) package.

There are also a few files:
- `requirements.txt` lists the package dependencies so that it is easy to regenerate an identical virtual environment on a different computer/server
- `config.py` stores the configuration settings (database, ...). **note** you can also use the environment variables defined there.
- `nomadapprun.py` launches the application (and perhaps other application tasks in future)

**Note - This directory is not used anymore since there is a functional API**: the `mockup` directory, inside `app` directory, contains a quick mockup server which returns the corresponding JSON output. Is needed for the GUI team to be able to test their requests live. The server returns _fake_ JSON output because there is still no data available for use in database, so no real queries to database.
