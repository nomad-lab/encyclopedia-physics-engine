Encyclopedia Physics Engine main repository
===

## Installation on a local machine (only Encyclopedia API and not full Encyclopedia stack)

1. clone the repository: `git clone git@gitlab.mpcdf.mpg.de:nomad-lab/encyclopedia-physics-engine.git`
2. create a python 3.x virtual environment: `pyvenv mypyvenv` and activate it: `source mypyvenv/bin/activate`
3. move to the folder containing the Flask web application:  `cd encyclopedia-physics-engine.git/rest-api-service`
4. install the required python modules, run: `pip install -r requirements.txt`
5. edit the `app/config.py` file and make the necessary changes to match your local database installation/credentials
6. run the web application: `python nomadapprun.py`
7. the web application should be up and running (e.g.: http://127.0.0.1:5000/v1.0/materials/10/calculations

Note: if the GUI is needed as well, please follow the install instructions provided in its repository: https://gitlab.mpcdf.mpg.de/nomad-lab/encyclopedia-gui

**Update** In order to have a full Encyclopedia stack (API, GUI, DB, sample data) installed and running on local machine, check this link (virtual machine and docker containers available to use): https://gitlab.mpcdf.mpg.de/nomad-lab/encyclopedia-infrastructure/tree/master/encyclopedia-stack

## Structure

- **DeliverableD2.1/** - contains the source code and config files (Nginx, Gunicorn, Supervisor) for Deliverable D2.1. No further changes will be done in this folder. An annotated tag "D2.1" is also present.

- **deploy/** - contains Ansible playbooks used for automatic deployment to testing/production machines

- **rest-api-service/** - contains the REST API service implementation

- **rest-api-specs/** - contains encyclopedia REST API specifications/documentation

- **rest-api-tutorial/** - contains a tutorial about how to use the Encyclopedia API from Python
