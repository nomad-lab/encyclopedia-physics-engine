# Mini-API

The current folder contains the Mini-API python source code for Deliverable D2.1 and the config files for Gunicorn, Nginx and Supervisor.

Technologies used:

- *Python* programming language with *Flask* microframework is used to develop the RESTful API interface
- *Nginx* is used as web server. It is designed to serve high-load webservices and it also includes handy features like reverse-proxy, load-balancing, basic authentication which we will use/need in the future
- *Gunicorn* (Green Unicorn) is used as a WSGI server to serve our main Python application. It is the recommended choice for Python web applications
- NOT USED FOR D2.1- *Supervisor* is used as a process manager. It provides a consistent interface through which long-running programs can be monitored and controlled

How to use the minimal API:
---------------------------

**Request**: GET

**URI**: /materials/search?element=ELEMENTS&page=PAGE-NUMBER&limit=ITEMS-PER-PAGE

**Parameters**:

- *element* - the elements for searching, separated by comma: eg. element=NH,C,Ba
- *page* - the number of page, 1 being the first page: eg. page=1
- *limit* - the number of entries shown on one page: eg. limit=25

**Note: all parameters are necessary!!!**



**Request example**:
~~~
enc-devel-nomad:~ # curl -i http://localhost/materials/search?element=C'&'page=1'&'limit=3
~~~


**Response example**:
~~~
HTTP/1.1 200 OK
Server: nginx/1.8.1
Date: Wed, 20 Apr 2016 09:54:50 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 791
Connection: keep-alive
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: HEAD, GET
Access-Control-Max-Age: 21600
Access-Control-Allow-Credentials: true
Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization

[
  {
    "xc_treatment_basis": "PBE+vdW",
    "space_group": 1,
    "nr_atoms": 26,
    "code_name": "FHI-aims 031011",
    "basis_set_type": "numeric AOs",
    "formula": "C<sub>6</sub>N<sub>4</sub>H<sub>14</sub>O<sub>2</sub>",
    "nr_results": 883
  },
  {
    "xc_treatment_basis": "PBE+vdW",
    "space_group": 1,
    "nr_atoms": 37,
    "code_name": "FHI-aims 031011",
    "basis_set_type": "numeric AOs",
    "formula": "CdC<sub>9</sub>N<sub>5</sub>H<sub>20</sub>O<sub>2</sub>",
    "nr_results": 883
  },
  {
    "xc_treatment_basis": "PBE+vdW",
    "space_group": 1,
    "nr_atoms": 28,
    "code_name": "FHI-aims 031011",
    "basis_set_type": "numeric AOs",
    "formula": "CaC<sub>6</sub>N<sub>4</sub>H<sub>15</sub>O<sub>2</sub>",
    "nr_results": 883
  }
]
~~~
